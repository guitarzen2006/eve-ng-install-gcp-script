##########################################
# Create image for nested vituralization #
##########################################
gcloud compute images create nested-ubuntu-xenial --source-image-family=ubuntu-1604-lts \
--source-image-project=ubuntu-os-cloud \
--licenses https://www.googleapis.com/compute/v1/projects/vm-options/global/licenses/enable-vmx

########################################################################################################################
########################################################################################################################
# Deployment Command
gcloud deployment-manager deployments create eve-ng --config eve-ng.yml \
--automatic-rollback-on-error


########################################################################################################################
sudo -i
wget -O - http://www.eve-ng.net/repo/install-eve.sh | bash -i
apt-get update && apt-get -y upgrade
shutdown -r 0

# COMPLETE REMAINDER OF INSTALLATION
ctrl-c
sudo -i



# DO NOT USE ddclient due to bug use dynu own tool
# See https://www.dynu.com/DynamicDNS/IPUpdateClient/Linux

URL='https://www.dynu.com/support/downloadfile/31'; FILE=`mktemp`; wget "$URL" -qO $FILE && sudo dpkg -i $FILE; rm $FILE

sudo cat << EOF > /etc/dynuiuc/dynuiuc.conf
username guitarzen2006
password w36MH08Vf&@to
location None
ipv4 true
ipv6 false
pollinterval 120
debug true          
quiet true      
EOF


systemctl restart dynuiuc.service
systemctl status dynuiuc.service
cat /var/log/dynuiuc.log


systemctl status dynuiuc.service

this is the answer:

dynuiuc.service - Dynu IP update client daemon
Loaded: loaded (/usr/lib/systemd/system/dynuiuc.service; disabled; vendor pre
Active: inactive (dead)

# I've found this solution, write the command:

systemctl enable dynuiuc.service

# and then the command:

systemctl start dynuiuc.service

last find the file:

/usr/lib/systemd/system/dynuiuc.service

and edit the line:

--pid_file /var/run/dynuiuc.pid
change it to:

--pid_file /run/dynuiuc.pid

shutdown -r 0
